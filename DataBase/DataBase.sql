-- PostgreSQL database dump

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

-- Create and fill the tables

CREATE TABLE specialties (
    id SERIAL PRIMARY KEY,
    name TEXT NOT NULL,
    value VARCHAR(20) NOT NULL UNIQUE
);

INSERT INTO specialties (id, name, value) VALUES 
(1, 'General', 'GEN'),
(2, 'Psicologia', 'PSI');

CREATE TABLE clinic (
    id SERIAL PRIMARY KEY,
    name VARCHAR(100) NOT NULL
);

INSERT INTO clinic (id, name) VALUES 
(1, 'Enconsulta');

CREATE TABLE users (
    id SERIAL PRIMARY KEY,
    name TEXT NOT NULL,
    lastname TEXT NOT NULL,
    ci_document TEXT NOT NULL,
    address TEXT NOT NULL,
    phone TEXT NOT NULL,
    username TEXT NOT NULL,
    birthday TIMESTAMP NOT NULL,
    email TEXT NOT NULL,
    familiar_phone TEXT NOT NULL,
    familiar_email TEXT NOT NULL
);

INSERT INTO users (id, name, lastname, ci_document, address, phone, username, birthday, email, familiar_phone, familiar_email) VALUES 
(1, 'Marcos', 'Alonso', '25889636', 'Valencia', '4144356688', 'marcosalonso', '1995-01-01 00:00:00', 'marcosalonso@yopmail.com', '4144356688', 'marcosalonso@yopmail.com'),
(2, 'Rios', 'Alexis', '26555555', 'Valencia', '4144356688', 'alexisrios', '1995-01-01 00:00:00', 'alexis@yopmail.com', '4144356688', 'marcosalonso@yopmail.com'),
(3, 'Guevara', 'Alejanda', '20111111', 'Valencia', '4124356688', 'aleguevara', '1995-01-01 00:00:00', 'guevara@yopmail.com', '4144356688', 'guevara@yopmail.com');

CREATE TABLE admins (
    id SERIAL PRIMARY KEY,
    username VARCHAR(100) NOT NULL UNIQUE,
    password TEXT NOT NULL
);

INSERT INTO admins (id, username, password) VALUES 
(1, 'superadmin', 'c177de664f8a0da095d5d917f029be0ddbaf41d83848ada154376a94278e3d36'),
(2, 'admin', 'ae98c868134418e5c25b1b623ce88a5cf2105862f6fdad5fe75715d4a2d9b3ac'),
(3, 'admin2', 'a7ebda64c6187264500ec0e7edac602b4954f116f562ceffaf2c28f95225855c');

CREATE TABLE specialists (
    id SERIAL PRIMARY KEY,
    name TEXT NOT NULL,
    lastname TEXT NOT NULL,
    speciality_id INT NOT NULL REFERENCES specialties(id),
    phone TEXT NOT NULL,
    email TEXT NOT NULL,
    username TEXT NOT NULL,
    ci_document VARCHAR(20) NOT NULL UNIQUE,
    gender VARCHAR(100),
    title VARCHAR(100)
);

INSERT INTO specialists (id, name, lastname, speciality_id, phone, email, username, ci_document, gender, title) VALUES 
(1, 'Beatriz', 'Hernández', 2, '1223456789', 'carlosduranh@yopmail.com', 'Beatriz_Hernández', '141345161', 'F', 'Dra.'),
(2, 'Jesús', 'Martínez', 1, '145689635', 'jesusmartinez1@yopmail.com', 'Jesús_Martínez', '11102650', 'M', 'Dr.'),
(3, 'José', 'Salas', 1, '1456896355', 'josesalas1@yopmail.com', 'José_Salas', '1456896344', 'M', 'Dr.'),
(4, 'Omar', 'Suáres', 2, '1456896355', 'omarsuares@yopmail.com', 'omarsuares', '44568963', 'M', 'Lic.'),
(5, 'Ana', 'Gomez', 2, '1456896355', 'anagomez@yopmail.com', 'anagomez', '12568963', 'F', 'Lic.');

CREATE TABLE slots (
    id SERIAL PRIMARY KEY,
    time_init VARCHAR(100) NOT NULL,
    time_end VARCHAR(100) NOT NULL
);

INSERT INTO slots (id, time_init, time_end) VALUES 
(1, '8:00 AM', '9:00 AM'),
(2, '9:00 AM', '10:00 AM'),
(3, '10:00 AM', '11:00 AM'),
(4, '11:00 AM', '12:00 M'),
(5, '12:00 M', '1:00 PM'),
(6, '1:00 PM', '2:00 PM'),
(7, '2:00 PM', '3:00 PM'),
(8, '3:00 PM', '4:00 PM');

CREATE TABLE medical_appointments (
    id SERIAL PRIMARY KEY,
    user_id INT NOT NULL REFERENCES users(id),
    specialist_id INT NOT NULL REFERENCES specialists(id),
    slot_id INT NOT NULL REFERENCES slots(id),
    clinic_id INT NOT NULL REFERENCES clinic(id),
    status VARCHAR(100) NOT NULL,
    appointment_date TIMESTAMP,
    fullname VARCHAR(100),
    appoinment_day VARCHAR(100),
    message TEXT,
    pacient_email VARCHAR(100),
    pacient_phone VARCHAR(100)
);

INSERT INTO medical_appointments 
(id, user_id, specialist_id, slot_id, clinic_id, status, appointment_date, fullname, appoinment_day, message, pacient_email, pacient_phone) 
VALUES 
(1, 1, 1, 1, 1, 'OPEN', '2022-10-01 00:00:00', 'Lirio Stalone', 'Lunes', 'Me duele la cabeza', 'liriosole@gmail.com', NULL),
(2, 2, 2, 3, 1, 'OPEN', '2022-10-01 00:00:00', 'Juan Rodriguez', 'Miercoles', 'dolor de rodilla', 'jrodriguez@gmail.com', NULL),
(11, 1, 5, 3, 1, 'OPEN', '2022-10-22 00:00:00', 'ssss', 'Lunes', 'ssss', 'lapiz@gmai', NULL),
(13, 1, 3, 3, 1, 'OPEN', '2022-10-22 00:00:00', 'jose morales', 'Miércoles', 'urgencia', 'email@gmail.com', NULL),
(14, 1, 3, 3, 1, 'OPEN', '2022-10-22 00:00:00', 'jesssica', 'Miércoles', '', 'enero@gmail', NULL),
(15, 1, 1, 1, 1, 'OPEN', '2022-10-22 00:00:00', 'jorge', 'Lunes', 'hjhh', 'jorge@hotmail.com', NULL),
(16, 1, 1, 2, 1, 'CONFIRMED', '2022-10-27 00:00:00', 'Jose Reyes', 'Thursday', 'Mensaje', 'josue@mail.com', NULL),
(17, 1, 1, 1, 1, 'NOT_CONFIRMED', '2022-10-27 00:00:00', 'Jose Reyes', 'Thursday', 'Mensaje', 'josue@yopmail.com', NULL),
(18, 1, 1, 3, 1, 'CONFIRMED', '2022-10-29 00:00:00', 'Jose Valbuena', 'Saturday', 'Mensjae', 'valbuena@mail.com', NULL),
(19, 1, 3, 4, 1, 'CONFIRMED', '2022-10-29 00:00:00', 'Jose Gonzales', 'Saturday', 'Mensaje para el especialista', 'gonzales@gmail.com', NULL),
(20, 1, 2, 2, 1, 'NOT_CONFIRMED', '2022-10-31 00:00:00', 'Maria Andrade', 'Monday', 'Mensaje', 'maria.andrade@mail.com', NULL),
(21, 1, 3, 2, 1, 'NOT_CONFIRMED', '2022-10-31 00:00:00', 'Jesus Marquez', 'Monday', 'Mensaje', 'jesus.marquez@mai.com', NULL),
(22, 1, 1, 2, 1, 'NOT_CONFIRMED', '2022-10-31 00:00:00', 'Marcos Duran', 'Monday', 'Mensaje', 'marcos@mail.com', NULL),
(23, 1, 4, 1, 1, 'NOT_CONFIRMED', '2022-10-31 00:00:00', 'Jose Barco', 'Monday', 'Mensaje', 'barco@mail.com', NULL),
(24, 1, 2, 5, 1, 'NOT_CONFIRMED', '2022-10-31 00:00:00', 'Daniel Sarcos', 'Monday', 'Mensaje', 'sarcos@mail.com', NULL),
(25, 1, 2, 5, 1, 'CONFIRMED', '2022-11-02 00:00:00', 'Mariam Vallejo', 'Wednesday', 'Mensaje', 'vallejo@mail.com', NULL),
(26, 1, 1, 7, 1, 'NOT_CONFIRMED', '2022-10-31 00:00:00', 'Jose Reyes', 'Monday', 'Mensaje', 'jose@mail.com', '0412-2345988'),
(27, 1, 4, 7, 1, 'NOT_CONFIRMED', '2022-11-01 00:00:00', 'Josue Martinez', 'Tuesday', 'Mensaje', 'josue@mail.com', '04121430425'),
(28, 1, 2, 4, 1, 'CONFIRMED', '2022-11-03 00:00:00', 'Jose Fuentes', 'Thursday', 'Mensaje', 'fuentes@mail.com', '04123456789'),
(29, 1, 3, 1, 1, 'NOT_CONFIRMED', '2022-11-04 00:00:00', 'perez', 'Friday', 'si', 'perez333@gmail.com', '04124144444'),
(30, 1, 3, 7, 1, 'NOT_CONFIRMED', '2022-11-03 00:00:00', 'mi nombre', 'Thursday', 'si', 'nomnre@gmail.com', '04141241220'),
(31, 1, 2, 4, 1, 'NOT_CONFIRMED', '2022-11-02 00:00:00', 'alex rome', 'Wednesday', 'sss', 'alero@gmail.com', '04244121212'),
(32, 1, 1, 3, 1, 'NOT_CONFIRMED', '2022-11-04 00:00:00', 'Josue', 'Friday', 'Mensaje', 'josue@mail.com', '04123456785'),
(33, 1, 4, 3, 1, 'CONFIRMED', '2022-12-09 00:00:00', 'ana carolina marquez', 'Friday', 'atención', 'anacaro@gmail.com', '04243215872'),
(34, 1, 4, 7, 1, 'CONFIRMED', '2022-11-02 00:00:00', 'Sergio Suares', 'Wednesday', 'psico', 'sersu@mail.com', '04263382116'),
(35, 1, 4, 4, 1, 'NOT_CONFIRMED', '2022-11-04 00:00:00', 'pedro perez', 'Friday', 'hola', 'pedrito@mail.com', '04144121212'),
(36, 1, 1, 1, 1, 'NOT_CONFIRMED', '2023-09-06 00:00:00', 'jesus', 'Wednesday', 'asa', 'jesau7m@gmail.com', '04144272577');

CREATE TABLE appointments_records (
    id SERIAL PRIMARY KEY,
    medical_appointment_id INT NOT NULL REFERENCES medical_appointments(id),
    movement VARCHAR(100) NOT NULL,
    record_date TIMESTAMP NOT NULL DEFAULT current_timestamp
);

-- Add the appointments_records data here

-- Indexes and constraints
-- Add necessary indexes and constraints here based on your requirements

CREATE TABLE roles (
    id SERIAL PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    description TEXT
);

ALTER TABLE admins
ADD COLUMN role_id INT REFERENCES roles(id);

CREATE TABLE menu_options (
    id SERIAL PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    url VARCHAR(255),
    parent_id INT REFERENCES menu_options(id) -- Esto permite tener submenús
);

CREATE TABLE role_menu (
    role_id INT NOT NULL REFERENCES roles(id),
    menu_option_id INT NOT NULL REFERENCES menu_options(id),
    PRIMARY KEY (role_id, menu_option_id)
);

CREATE UNIQUE INDEX idx_unique_role_name ON roles(name);
CREATE UNIQUE INDEX idx_unique_menu_option_name ON menu_options(name);

INSERT INTO roles (name, description) VALUES
('admin', 'Administrador del sistema'),
('secretary', 'Secretario/a'),
('specialist', 'Especialista médico');

INSERT INTO menu_options (name, url) VALUES
('Especialistas', './specialists.html'),
('Admins', './admins.html'),
('Citas', './appoinments.html');

-- Especialistas tendrán acceso a citas nada más
INSERT INTO role_menu (role_id, menu_option_id) VALUES
(3, 3);

-- Secretary tendrá acceso a citas y especialistas
INSERT INTO role_menu (role_id, menu_option_id) VALUES
(2, 1),
(2, 3);

-- Admin tendrán acceso a todas las opciones de menú
INSERT INTO role_menu (role_id, menu_option_id) VALUES
(1, 1),
(1, 2),
(1, 3);




-- PostgreSQL database dump complete