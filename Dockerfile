# Usa una imagen base oficial de Python
FROM python:3.11

# Establece un directorio de trabajo en el contenedor
WORKDIR /app

# Copia los archivos de requisitos y los instala
COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

# Copia los archivos fuente de la aplicación al contenedor
COPY src/ /app

# Expone el puerto en el que se ejecutará la aplicación
EXPOSE 10000

# Comando para ejecutar la aplicación
CMD ["python", "/app/app.py"]