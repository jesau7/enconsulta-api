## Documentación del microservicio

https://enconsulta-api.onrender.com/api/docs#/clinics/get_clinics



### Enconsulta API

Servicio API REST para pagina de Enconsulta.

### Requisitos

#### En sistemas operativos basados en Linux:

- Python 3

- pip

Puedes instalar ambos con el siguiente comando:

```bash

sudo  apt  install  python3  python3-pip

```

#### En Windows:

- [Python](https://www.python.org/downloads/)

- pip

Hay dos opciones para instalar Python y pip en Windows:

**Opción 1**: Utilizar Chocolatey, un gestor de paquetes para Windows. Si no tienes Chocolatey, puedes instalarlo siguiendo las instrucciones en su página oficial.

Una vez que tengas Chocolatey, ejecuta el siguiente comando para instalar Python y pip:

```bash

choco  install  python

```

**Opción 2**: Descargar Python directamente desde su página oficial y seguir las instrucciones de instalación.

## Configuración inicial

Clona este repositorio en tu máquina local:

```bash

git  clone https://gitlab.com/jesau7/enconsulta-api

cd enconsulta-api

```

## Instalación de dependencias

Instala las dependencias necesarias con pip:

```bash

pip  install  --no-cache-dir  -r  requirements.txt

```

## Ejecución de la aplicación

Ejecuta la aplicación con el siguiente comando:

```bash

python  src/app.py

```

Tu aplicación ahora debería estar ejecutándose en http://localhost:10000.

## Documentación adicional

- [Flask](https://flask.palletsprojects.com/)

- [Python](https://www.python.org/)

- [PostgreSQL](https://www.postgresql.org/)

## Documentación de la API
[Aquí](http://localhost:10000/api/docs)

## Licencia

MIT License
