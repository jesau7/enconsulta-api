import datetime
import hashlib
from operator import eq
from string import Template
from flask import Flask, jsonify, request, render_template
from flask_cors import CORS
from flask_mail import Mail, Message
from flask_marshmallow import Marshmallow
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import create_engine
from sqlalchemy import desc
from sqlalchemy.orm import sessionmaker
from sqlalchemy import Date
from flask_jwt_extended import JWTManager, create_access_token, jwt_required, get_jwt_identity


database_url = 'postgresql+psycopg2://admin:ppXJA1TePlTVxh2SOls4GNufvVZ1YUaZ@dpg-ckblhjmct0pc73eu1deg-a.oregon-postgres.render.com/consultoriodb'
engine = create_engine(database_url)
Session = sessionmaker(bind = engine)
session = Session()
app = Flask(__name__)
mail = Mail(app)
CORS(app, resources={r"/*": {"origins": "*"}})
email = 'enconsultasalud@gmail.com'
# BD string
app.config['SQLALCHEMY_DATABASE_URI'] = database_url
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
# Mailer config
app.config['MAIL_SERVER']='smtp.mailtrap.io'
app.config['MAIL_PORT'] = 2525
app.config['MAIL_USERNAME'] = '44737cdea6f30e'
app.config['MAIL_PASSWORD'] = 'b6e26a8f62a83f'
app.config['MAIL_USE_TLS'] = True
app.config['MAIL_USE_SSL'] = False

db = SQLAlchemy(app)
ma = Marshmallow(app)
mail = Mail(app)

app.config['JWT_SECRET_KEY'] = 'super-secret'  # Esto debe ser un valor secreto y aleatorio en producción
jwt = JWTManager(app)

### Models and schemas

## Clinic model and schema
class Clinic(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(70), unique=True)

    def __init__(self, name):
        self.name = name


class ClinicSchema(ma.Schema):
    class Meta:
        fields = ('id', 'name')


clinic_schema = ClinicSchema()
clinics_schema = ClinicSchema(many=True)

## admins model an schema
class Admins(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(70), unique=True)
    password = db.Column(db.String(70))
    role_id = db.Column(db.Integer, db.ForeignKey('roles.id'), nullable=False)

    def __init__(self, username, password, role_id=1):
        self.username = username
        self.password = password
        self.role_id = role_id

class AdminsSchema(ma.Schema):
    class Meta:
        fields = ('id', 'username', 'password', 'role_id')
        
admin_schema = AdminsSchema()
admins_schema = AdminsSchema(many=True)

# specialities model and schema
class Specialties(db.Model):
    
    # __tablename__ = 'specialties'
    
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String())
    value = db.Column(db.String(), unique=True)
    specialists = db.relationship('Specialists', backref='specialties')

    def __init__(self, name, value):
        self.name = name
        self.value = value

class SpecialtiesSchema(ma.Schema):
    class Meta:
        fields = ('id', 'name', 'value')
        
speciality_schema = SpecialtiesSchema()
specialities_schema = SpecialtiesSchema(many=True)
        
# slots model and schema
class Slots(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    time_init = db.Column(db.String(70))
    time_end = db.Column(db.String(70))
    
    def __init__(self, time_init, time_end):
        self.time_init = time_init
        self.time_end = time_end

class SlotsSchema(ma.Schema):
    class Meta:
        fields = ('id', 'time_init', 'time_end')

slot_schema = SlotsSchema()
slots_schema = SlotsSchema(many=True)

# users model and schema 
class Users(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(70))
    lastname = db.Column(db.String(70))
    address = db.Column(db.String(70))
    phone = db.Column(db.String(70))
    birthday = db.Column(Date)
    email = db.Column(db.String(70), unique=True)
    username = db.Column(db.String(70), unique=True)
    ci_document = db.Column(db.String(70), unique=True)
    familiar_phone = db.Column(db.String(70))
    familiar_email = db.Column(db.String(70))
    
class UsersSchema(ma.Schema):
    class Meta:
        fields = ('id', 'name', 'lastname', 'address', 'phone', 'birthday', 'email', 'username', 'ci_document', 'familiar_phone', 'familiar_email')
        
user_schema = UsersSchema()
users_schema = UsersSchema(many=True)

# specialits model and schema
class Specialists(db.Model):
    
    # __tablename__ = 'specialists'
    
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(70))
    lastname = db.Column(db.String(70))
    phone = db.Column(db.String(70))
    email = db.Column(db.String(70), unique=True)
    username = db.Column(db.String(70), unique=True)
    ci_document = db.Column(db.String(70), unique=True)
    gender = db.Column(db.String(70))
    title = db.Column(db.String(70))
    speciality_id = db.Column(db.Integer, db.ForeignKey('specialties.id'))
    medical_appointments = db.relationship('MedicalAppointments', backref='medical_appointments')
    
class SpecialistsSchema(ma.Schema):
    class Meta:
        fields = ('id', 'name', 'lastname', 'phone', 'email', 'username', 'ci_document', 'speciality_id', 'gender', 'title')
    
specialist_schema = SpecialistsSchema()
specialists_schema = SpecialistsSchema(many=True)

class MedicalAppointments(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    appointment_date = db.Column(Date)
    status = db.Column(db.String(70))
    fullname = db.Column(db.String(70))
    appoinment_day = db.Column(db.String(70))
    message = db.Column(db.String(256))
    pacient_email = db.Column(db.String(70))
    pacient_phone = db.Column(db.String(70))
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    specialist_id = db.Column(db.Integer, db.ForeignKey('specialists.id'), nullable=False)
    slot_id = db.Column(db.Integer, db.ForeignKey('slots.id'), nullable=False)
    clinic_id = db.Column(db.Integer, db.ForeignKey('clinic.id'), nullable=False)
    
class MedicalAppointmentsSchema(ma.Schema):
    class Meta:
        fields = ('id', 'appointment_date', 'user_id', 'specialist_id', 'slot_id', 'clinic_id', 'status', 'fullname', 'appoinment_day', 'message', 'pacient_email', 'pacient_phone')
        
medical_appointment_schema = MedicalAppointmentsSchema()
medical_appointments_schema = MedicalAppointmentsSchema(many=True)

class Roles(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(70), unique=True)
    
class RolesSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Roles
        
class MenuOptions(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(70), unique=True)
    url = db.Column(db.String(70), unique=False)

class MenuOptionsSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = MenuOptions

class RoleMenu(db.Model):
    role_id = db.Column(db.Integer, db.ForeignKey('roles.id'), primary_key=True)
    menu_option_id = db.Column(db.Integer, db.ForeignKey('menu_options.id'), primary_key=True)

class RoleMenuSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = RoleMenu
        
@app.route('/', methods=['GET'])
def index():
    return jsonify({'message': 'Welcome to Enconsulta API'})


@app.route('/api/docs')
def get_docs():
    print('sending docs')
    return render_template('swaggerui.html')

@app.route('/clinics', methods=['GET'])
def get_tasks():
  all_clinics = Clinic.query.all()
  result = clinics_schema.dump(all_clinics)
  return jsonify(result)

@app.route('/clinic', methods=['Post'])
@jwt_required()
def create_task():
  title = request.json['title']
  description = request.json['description']

  new_task= Clinic(title, description)

  db.session.add(new_task)
  db.session.commit()

  return clinic_schema.jsonify(new_task)

@app.route('/clinics/<id>', methods=['GET'])
def get_task(id):
  task = Clinic.query.get(id)
  return clinic_schema.jsonify(task)

@app.route('/clinics/<id>', methods=['PUT'])
@jwt_required()
def update_task(id):
  task = Clinic.query.get(id)

  title = request.json['title']
  description = request.json['description']

  task.title = title
  task.description = description

  db.session.commit()

  return clinic_schema.jsonify(task)

@app.route('/clinics/<id>', methods=['DELETE'])
@jwt_required()
def delete_task(id):
  task = Clinic.query.get(id)
  db.session.delete(task)
  db.session.commit()
  return clinic_schema.jsonify(task)

@app.route('/users', methods=['POST'])
def create_user():
    ci_document = request.json['ci_document']
    
    current_user = Users.query.filter_by(ci_document=ci_document).first()
    if current_user is not None:
        return jsonify({'code': -1,'message': 'User already exists'})
    
    user_data = request.get_json()
    new_user = Users(**user_data)
    
    db.session.add(new_user)
    db.session.commit()
    
    return user_schema.jsonify(new_user)

# method GET user by id 
@app.route('/users/<id>', methods=['GET'])
@jwt_required()
def get_user_by_id(id):
    user = Users.query.get(id)
    return user_schema.jsonify(user)


# method GET user by document 
@app.route('/users/document/<ci_document>', methods=['GET'])
@jwt_required()
def get_user_by_document(ci_document):
    user = Users.query.filter_by(ci_document=ci_document).first()
    return user_schema.jsonify(user)

@app.route('/slots')
def get_slots():
    slots = Slots.query.all()
    return slots_schema.jsonify(slots)

@app.route('/specialties')
def get_specialities():
    specialties = Specialties.query.all()
    return specialities_schema.jsonify(specialties)

@app.route('/specialists')
def get_specialist():
    specialists = Specialists.query.all()
    return specialists_schema.jsonify(specialists)

@app.route('/specialists/<id>')
@jwt_required()
def get_specialists(id):
    specialist = Specialists.query.filter_by(id=id).first()
    return specialist_schema.jsonify(specialist)

@app.route('/specialists', methods=['POST'])
@jwt_required()
def create_specialists():
    new_specialist = Specialists(**request.get_json())
    db.session.add(new_specialist)
    db.session.commit()
    return specialist_schema.jsonify(new_specialist)

@app.route('/specialists/<id>', methods=['PUT'])
@jwt_required()
def update_specialists(id):
    Specialists.query.filter_by(id=id).update(dict(**request.get_json()))
    db.session.commit()
    specialist = Specialists.query.get(id)
    return specialist_schema.jsonify(specialist)

@app.route('/specialists/<id>', methods=['DELETE'])
@jwt_required()
def delete_specialists(id):
    specialist = Specialists.query.get(id)
    db.session.delete(specialist)
    db.session.commit()
    return specialist_schema.jsonify(specialist)

@app.route('/specialists/speciality/<value>')
def get_specialists_by_spececiality(value):
    speciality = Specialties.query.filter_by(value=value).first()
    
    if speciality is None:
        return jsonify({'value': value, 'message': 'Speciality not found', 'code': -1})
    
    specialists = Specialists.query.filter_by(speciality_id = speciality.id).all()
    return specialists_schema.jsonify(specialists)

@app.route('/medical_appoinments')
def get_medical_appoinments():
    data = db.session.query(MedicalAppointments, Specialists, Slots).filter(MedicalAppointments.specialist_id == Specialists.id).\
        filter(MedicalAppointments.slot_id == Slots.id)\
            .order_by(desc(MedicalAppointments.appointment_date)).all()
    result = []
    for row in data:
        medical_appointments, specialist, slot = row
        curr_dict = {
            "specialist_name": specialist.name + ' ' + specialist.lastname, 
            "id": medical_appointments.id, 
            "fullname": medical_appointments.fullname, 
            "appoinment_day": medical_appointments.appoinment_day, 
            "message": medical_appointments.message, 
            "pacient_email": medical_appointments.pacient_email, 
            "pacient_phone": medical_appointments.pacient_phone, 
            "status": medical_appointments.status, 
            "appointment_date": medical_appointments.appointment_date,
            "time": slot.time_init
            }
        result.append(curr_dict)
        
    db.session.commit()
    return jsonify(result)

@app.route('/medical_appoinments', methods = ['POST'])
def create_medical_appoinments():
    json_data = request.get_json()
    
    #Validate if medical_appoinment exists
    specialist_id = request.json['specialist_id']
    appointment_date = request.json['appointment_date']
    slot_id = request.json['slot_id']
    medical_appointment = MedicalAppointments.query.filter_by(specialist_id=specialist_id, appointment_date=appointment_date, slot_id=slot_id).first()
    if medical_appointment is not None:
        return jsonify({'code': -1, 'message': 'Medical appointment already exists'})
    
    new_medical_appoinment = MedicalAppointments(**json_data)
    db.session.add(new_medical_appoinment)
    db.session.commit()
    
    pacient_email = request.json['pacient_email']
    fullname = request.json['fullname']
    pacient_phone = request.json['pacient_phone']
    slot = Slots.query.filter_by(id=slot_id).first()
    
    body = Template("""
                   Mensaje: Ha solicitado una nueva cita medica para el dia:
                   Fecha: $appointment_date, Hora: $time_init
                   
                   A nombre de: $name
                   Contacto:
                   Teléfono: $pacient_phone.
                   E-mail: $pacient_email
                   
                   para confirmar su cita por favor ingrese al siguiente link:
                   http://localhost:5500/life-care/confirmation.html?app_id=$id
                   """)
   
    msg = Message("Nueva cita por confirmar", sender = "enconsultasalud@gmail.com", recipients = [pacient_email])
    msg.body = body.substitute(name=fullname, appointment_date=appointment_date, time_init=slot.time_init, id=new_medical_appoinment.id, pacient_email=pacient_email, pacient_phone=pacient_phone)
    mail.send(msg)
   
    return medical_appointment_schema.jsonify(new_medical_appoinment)

@app.route('/medical_appoinments/<id>', methods = ['DELETE'])
def delete_medical_appoinments(id):
    medical_appointment = MedicalAppointments.query.get(id)
    db.session.delete(medical_appointment)
    db.session.commit()
    return medical_appointment_schema.jsonify(medical_appointment)

@app.route("/send-message", methods=['POST'])
def send_message():
   subject = request.json['subject']
   from_email = request.json['from_email']
   name = request.json['name']
   message = request.json['message']
   phone = request.json['phone']
   
   body = Template("""
                   Mensaje: $message.
                   
                   Nombre: $name
                   Telf.: $phone
                   Correo de contacto: $from_email
                   """)
   
   msg = Message(subject, sender = from_email, recipients = [email])
   msg.body = body.substitute(message=message, name=name, phone=phone, from_email=from_email)
   mail.send(msg)
   return jsonify({ "message":"Sent" })

@app.route("/login", methods=['POST'])
def login():
   username = request.json['username']
   password = request.json['password']
   hashed_password = hash256_password(password)
   admin = Admins.query.filter_by(username=username).first()
   print("admin: {}".format(hashed_password))
   
   if not admin or not eq(admin.password, hashed_password):
       return jsonify({'message': 'Login failed', 'code': -1}), 401
   
   role = Roles.query.filter_by(id=admin.role_id).first()
   print("role: {}".format(role))
   access_token = create_access_token(identity={'admin_id': admin.id, 'role': role.name}, expires_delta=datetime.timedelta(hours=1))
   return jsonify({'code': 0, 'message': 'Login success', "access_token": access_token})

# List admins records
@app.route('/admins')
@jwt_required()
def get_admins():
    all_admins = Admins.query.all()
    result = admins_schema.dump(all_admins)
    return jsonify(result)

@app.route('/admins/<id>')
@jwt_required()
def get_admin(id):
    admin = Admins.query.get(id)
    return admin_schema.jsonify(admin)    

# Add admin users to database
@app.route('/admins', methods=['POST'])
@jwt_required()
def create_admin():
    username = request.json['username']
    password = request.json['password']
    role_id = request.json['role_id']
    admin = Admins.query.filter_by(username=username).first()
    if admin is not None:
        return jsonify({'code': -1, 'message': 'Admin already exists'})
    
    if password:
        hashed_password = hash256_password(password)
    new_admin = Admins(username, password=hashed_password, role_id=role_id)
    db.session.add(new_admin)
    db.session.commit()
    return admin_schema.jsonify(new_admin)

# Delete admin users from database
@app.route('/admins/<id>', methods=['DELETE'])
@jwt_required()
def delete_admin(id):
    admin = Admins.query.get(id)
    db.session.delete(admin)
    db.session.commit()
    return admin_schema.jsonify(admin)

# update admin users from database
@app.route('/admins/<id>', methods=['PUT'])
@jwt_required()
def update_admin(id):
    password = request.json.get('password')
    if password:
        hashed_password = hash256_password(password)
        request.json['password'] = hashed_password
    Admins.query.filter_by(id=id).update(request.json)
    db.session.commit()
    admin = Admins.query.get(id)
    return admin_schema.jsonify(admin)

@app.route('/medical_appoinments/<id>', methods=['PATCH'])
@jwt_required()
def patch_medical_appoinment(id):
    MedicalAppointments.query.filter_by(id=id).update(dict(**request.get_json()))
    db.session.commit()
    medical_appointments = MedicalAppointments.query.get(id)
    return medical_appointment_schema.jsonify(medical_appointments)

@app.route('/roles', methods=['GET', 'POST'])
@jwt_required()
def manage_roles():
    if request.method == 'GET':
        all_roles = Roles.query.all()
        return RolesSchema(many=True).jsonify(all_roles)
    elif request.method == 'POST':
        new_role = Roles(**request.get_json())
        db.session.add(new_role)
        db.session.commit()
        return RolesSchema().jsonify(new_role)

@app.route('/menu_options', methods=['GET', 'POST'])
@jwt_required()
def manage_menu_options():
    if request.method == 'GET':
        all_menu_options = MenuOptions.query.all()
        return MenuOptionsSchema(many=True).jsonify(all_menu_options)
    elif request.method == 'POST':
        new_menu_option = MenuOptions(**request.get_json())
        db.session.add(new_menu_option)
        db.session.commit()
        return MenuOptionsSchema().jsonify(new_menu_option)

@app.route('/role_menu', methods=['POST'])
@jwt_required()
def create_role_menu():
    new_role_menu = RoleMenu(**request.get_json())
    db.session.add(new_role_menu)
    db.session.commit()
    return RoleMenuSchema().jsonify(new_role_menu)

@app.route('/menu_options/<role_name>', methods=['GET'])
@jwt_required()
def get_menu_options(role_name):
    role = Roles.query.filter_by(name=role_name).first()
    if not role:
        return jsonify({'message': 'Role not found', 'code': -1}), 404

    role_menus = db.session.query(RoleMenu, MenuOptions).filter(
        RoleMenu.role_id == role.id,
        RoleMenu.menu_option_id == MenuOptions.id
    ).all()

    menu_options = [{"name": menu_option.name, "url": menu_option.url} for role_menu, menu_option in role_menus]
    return jsonify({'menu_options': menu_options})


def hash256_password(password):
    salt = "enconsulta_aleatorio"
    # Se concatena la contraseña con el salt para añadir aleatoriedad
    password_con_salt = password + salt
    # Se codifica a bytes la cadena resultante
    password_bytes = password_con_salt.encode('utf-8')
    # Se aplica la función hash sha256
    sha256 = hashlib.sha256()
    sha256.update(password_bytes)
    # Se retorna la contraseña encriptada y el salt utilizado
    return sha256.hexdigest()

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=10000, debug=True)
